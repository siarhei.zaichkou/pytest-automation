from selenium import webdriver


class TestW3C:
    def test_w3c_title(self):
        driver = webdriver.Chrome()
        driver.get("https://www.w3.org/")

        assert driver.title == "W3C", "The title is not correct"
