from selenium import webdriver


class TestMarvel:
    def test_marvel_title(self):
        driver = webdriver.Chrome()
        driver.get("https://www.marvel.com/")

        assert driver.title == "Marvel.com | The Official Site for Marvel Movies, Characters, Comics, TV",\
            "The title is not correct"
