import dotenv
import os
import pickle
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

dotenv.load_dotenv()


class TestOrangeHrm:
    login = os.getenv('login')
    password = os.getenv('password')
    BASE_URL = " https://opensource-demo.orangehrmlive.com/web/index.php/auth/"
    USERNAME_FIELD = ("xpath", "//input[@name='username']")
    PASSWORD_FIELD = ("xpath", "//input[@name='password']")
    LOGIN_BUTTON = ("xpath", "//button[@type='submit']")
    MY_INFO_BUTTON = ("xpath", "//span[text()='My Info']")
    EMPLOYEE_CONTENT_DIV = ("xpath", "//div[@class='orangehrm-edit-employee-content']")

    def setup_method(self):
        self.driver = webdriver.Chrome()
        self.wait = WebDriverWait(self.driver, 10, poll_frequency=1)

    def test_login(self):
        self.driver.get(f"{self.BASE_URL}{"login"}")
        self.wait.until(EC.visibility_of_element_located(self.USERNAME_FIELD)).send_keys(self.login)
        self.wait.until(EC.visibility_of_element_located(self.PASSWORD_FIELD)).send_keys(self.password)
        self.driver.find_element(*self.LOGIN_BUTTON).click()
        pickle.dump(self.driver.get_cookies(), open(os.getcwd() + "/hw_2/cookies.pkl", "wb"))

        assert self.driver.current_url == "https://opensource-demo.orangehrmlive.com/web/index.php/dashboard/index"
        assert "Orange" in self.driver.title

    def test_my_info(self):
        self.driver.get(f"{self.BASE_URL}{"login"}")
        self.driver.delete_all_cookies()
        cookies = pickle.load(open(os.getcwd() + "/hw_2/cookies.pkl", "rb"))
        for cookie in cookies:
            self.driver.add_cookie(cookie)
        self.driver.refresh()

        assert self.driver.current_url == "https://opensource-demo.orangehrmlive.com/web/index.php/dashboard/index"

        self.wait.until(EC.visibility_of_element_located(self.MY_INFO_BUTTON)).click()
        self.wait.until(EC.visibility_of_element_located(self.EMPLOYEE_CONTENT_DIV))

    def teardown_method(self):
        self.driver.close()
