import requests


class TestJSonPlaceholderApi:
    BASE_URL = "https://jsonplaceholder.typicode.com/"

    def test_create_post(self):
        payload = {
            'title': 'Super-Duper-Post',
            'body': 'Post_Post_Post',
            'userId': 2,
        }
        response = requests.post(url=f"{self.BASE_URL}posts", json=payload)

        assert response.status_code == 201, "Status code is not 201"
        post = response.json()
        assert post.get('title') == payload['title']
        assert post.get('body') == payload['body']
        assert post.get('userId') == payload['userId']

    def test_delete_post(self):
        response = requests.delete(url=f"{self.BASE_URL}posts/1")
        assert response.status_code == 200, "Status code is not 200"
        assert response.json() == {}

    def test_get_non_existent_post(self):
        response = requests.get(url=f"{self.BASE_URL}posts/666")
        assert response.status_code == 404, "Status code is not 404"
